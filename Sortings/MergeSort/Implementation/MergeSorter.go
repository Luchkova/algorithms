package merge

/*Sorter provides sorting using MergeSort algorithm **/
type Sorter struct {
	//Input array, holds result as well
	Array []int
	//temp array
	helper []int
}

/*Sort executes sorting**/
func (sorter *Sorter) Sort() {
	//Create temp array with size of input one
	sorter.helper = make([]int, len(sorter.Array))

	sorter.split(0, len(sorter.Array)-1)
}

//Splits array into halves
func (sorter *Sorter) split(start int, end int) {
	if start < end {
		middle := (start + end) / 2
		//split left part
		sorter.split(start, middle)
		//split right part
		sorter.split(middle+1, end)
		//no merge splitted arrays
		sorter.merge(start, middle, end)
	}
}

func (sorter *Sorter) merge(start int, middle int, end int) {
	//instead of creating new arrays use temp array
	for i := start; i <= end; i++ {
		sorter.helper[i] = sorter.Array[i]
	}

	i, j, current := start, middle+1, start

	for i <= middle && j <= end {
		//Now choose whatever array contains smaller values and put it to result
		if sorter.helper[i] <= sorter.helper[j] {
			sorter.Array[current] = sorter.helper[i]
			i++
		} else {
			sorter.Array[current] = sorter.helper[j]
			j++
		}
		current++
	}

	//Add items of left array if any
	for i <= middle {
		sorter.Array[current] = sorter.helper[i]
		i++
		current++
	}

	//And items of right array if any
	for j <= end {
		sorter.Array[current] = sorter.helper[j]
		j++
		current++
	}

}
