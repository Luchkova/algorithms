export class RadixSort {
	
	// v1 for unsigned integers
    
    public static radixSortForUnsigned(array: number[]) {
        let max = this.getMaxValue(array);
        this.countSortForUnsigned(array, [array], 1, max); // 1 -> let's check least significant digit of all numbers
    }

    private static countSortForUnsigned(array: number[], oldCounter: number[][], exp: number, max: number) {
        if (Math.floor(max/exp) == 0) { // no more digits to check
            let j = 0;            
            for (let property of oldCounter) { // put sorted numbers to initial array
                if (property.length) {
                    for (let item of property) {
                        array[j++] = item;
                    }
                }
            }
            return;
        }

        let newCounter: number[][] = [];
        for (let i = 0; i <= 9; i++) { // array per digit
            newCounter.push([]);
        }

        for (let property of oldCounter) {
            if (property.length) {
                for (let item of property) {
                    let digit =(Math.floor(item/exp) % 10);
                    newCounter[digit].push(item);
                }
            }
        }

        this.countSort(array, newCounter, exp*10, max); // let's check next least significant digit
    }

    private static getMaxValue(array: number[]): number {
        if (array) {
            let max = array[0];
            for (let i = 1; i < array.length; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            return max;
        } else {
            return null;
        }
        
    }

    // v2 for signed integers
    
    public static radixSortForSigned(array: number[]) {
        let max = this.getMaxAbsoluteValue(array);
        this.countSortForSigned(array, [array], 1, max); // 1 -> let's check least significant digit of all numbers
    }

    private static countSortForSigned(array: number[], oldCounter: number[][], exp: number, max: number) {
        if (Math.floor(max*10/exp) == 0) { // no more digits to check and signs are checked too
            let j = 0;            
            for (let property of oldCounter) { // put sorted numbers to initial array
                if (property.length) {
                    for (let item of property) {
                        array[j++] = item;
                    }
                }
            }
            return;
        }

        let newCounter: number[][] = [];
        for (let i = 0; i <= 10; i++) { // array per digit and array for signed numbers
            newCounter.push([]);
        }

        for (let i=0; i < oldCounter.length; i++) {
            if (oldCounter[i].length) {
                for (let item of oldCounter[i]) {
                    let digit =(Math.floor( Math.abs(item)/exp ) % 10);
                    if (digit == 0 && item < 0 && i > 1) { // if next "digit" is "-"
                        newCounter[0].unshift(item); // put on the top of array to get reverse order of signed numbers with the same number of digits
                    } else {
                        newCounter[digit+1].push(item);
                    }
                }
            }
        }

        this.countSort(array, newCounter, exp*10, max); // let's check next least significant digit
    }

    private static getMaxAbsoluteValue(array: number[]): number {
        if (array) {
            let max = Math.abs(array[0]);
            for (let i = 1; i < array.length; i++) {
                if (Math.abs(array[i]) > max) {
                    max = Math.abs(array[i]);
                }
            }
            return max;
        } else {
            return null;
        }        
    }
}